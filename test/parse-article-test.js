var parseArticle = require('../lib/parse-article');
var expect = require('expect');

describe('ParseArticle', function () {
    it('Is a function', function () {
        expect(parseArticle).toBeA(Function);
    });

    it('Returns an image', function () {
        var content = '<div><img src="image.png"/></div>';

        var attrs = parseArticle(content, 'http://link');

        expect(attrs).toBeAn(Object);
        expect(attrs.image).toEqual('http://link/image.png');
    });

    it('Returns a video', function () {
        var content = '<p>something</p> <p><iframe src="http://youtube.com/embed/2J6237qvypg?iv_load_policy=3&#038;modestbranding=1&#038;showinfo=0" width="640" height="349" frameborder="0" allowfullscreen=""></iframe></p>';

        var attrs = parseArticle(content, 'http://link');

        expect(attrs).toBeAn(Object);
        expect(attrs.youtubeId).toEqual('2J6237qvypg');
    });
})