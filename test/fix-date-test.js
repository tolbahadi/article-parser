'use strict';

var expect = require('expect');
var moment = require('moment');
var fixArticleDate = require('../lib/fix-article-date');

describe('FixArticleDate', function () {
    it('returns the same date', function () {
        var originDate = moment();

        var correctDate = fixArticleDate(originDate);

        expect(correctDate - originDate.toDate()).toEqual(0)
    });

    it('Modifies date', function () {
        var originDate = moment();
        var expectedDate = new Date(originDate.toDate());
        originDate.subtract(2, 'hours');
        var correctDate = fixArticleDate(originDate.toDate());
        expect(correctDate - expectedDate).toEqual(0);
    });
});
