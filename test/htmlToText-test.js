'use strict';

require('should');

var htmlToText = require('../lib/html-to-text'),
    fs = require('fs'),
    _ = require('underscore');

describe('htmlToText module', function () {
    it('#trim1', function () {
        htmlToText('    ').should.eql('');
    });
    it('#trim2', function () {
        htmlToText('a    ').should.eql('a');
    });
    it('#trim3', function () {
        htmlToText('    b').should.eql('b');
    });
    it('#muliple spaces', function () {
        htmlToText('a    b').should.eql('a b');
    });
    it('#muliple spaces2', function () {
        htmlToText('a  \n  b').should.eql('a\nb');
    });
    it('#muliple spaces3', function () {
        htmlToText('a  \n  \n  b').should.eql('a\nb');
    });
    it('#&nbsp;', function () {
        htmlToText('a&nbsp;b').should.eql('a b');
    });
    it('#&nbsp2', function () {
        htmlToText('a&nbsp;\n \n &nbsp; \n \n  &nbsp;&nbsp;b').should.eql('a\nb');
    });


    var list = _.range(1, 6);

    _.each(list, function (index) {
        it('#' + index, function () {
            var html = fs.readFileSync(__dirname + '/htmlToText/html' + index, 'utf8');
            var text = fs.readFileSync(__dirname + '/htmlToText/text' + index, 'utf8');
            htmlToText(html).should.eql(text);
        });
    });
});