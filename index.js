
var parseArticle = require('./lib/parse-article');

module.exports = {
    parseArticle: parseArticle,
    fixDate: require('./lib/fix-article-date')
};
