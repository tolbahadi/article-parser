'use strict';

var url = require('url');

var cheerio = require('cheerio');

var sanitizeContent = require('./sanitize-article-content');

// get Article image
function getArticleImage ($content, link) {
    var $img = $content.find('img').first();
    var imgUrl;

    if ($img.length) {
        imgUrl = $img.attr('src');

        if (link && imgUrl && !/http:\/\//.test(imgUrl)) {
            var urlParts = url.parse(link);
            var baseUrl = urlParts.protocol + '//' + urlParts.host;
            imgUrl = url.resolve(baseUrl, imgUrl);
        }

        $img.remove();
    }
    return imgUrl;
}

function getYoutubeId ($content) {
    var videoSrc = $content.find('iframe').first().attr('src');

    if (!videoSrc) {
        videoSrc = $content.find('embed').first().attr('src');
    }

    if (!videoSrc) {
        videoSrc = $content.find('object').first().attr('data');
    }

    if (videoSrc) {
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = videoSrc.match(regExp);
        if (match && match[7].length === 11) {
            return match[7];
        }
    }
}

function parseArticle (content, link) {
    var $content = cheerio('<div></div>').append(content);

    var youtubeId = getYoutubeId($content),
        image = getArticleImage($content, link);

    var attrs = {
        content: sanitizeContent($content, link)
    };

    if (youtubeId) {
        attrs.youtubeId = youtubeId;
    }

    if (image) {
        attrs.image = image;
    }

    return attrs;
}

module.exports = parseArticle;
