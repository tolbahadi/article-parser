'use strict';

var moment = require('moment');

/**
 * Converts a date into this hour's date
 * This solves timezone differences issues
 */
module.exports = function (originDate) {
    originDate = moment(originDate);

    var now = moment();

    var correctDate = moment();

    correctDate.minutes(originDate.minutes());

    if (correctDate > now) {
        correctDate.subtract(1, 'hours');
    }

    return correctDate.toDate();
};
