'use strict';

var unescapeHtml = require('./unescape-html');

module.exports = function (html) {
    html = html || '';

    html = unescapeHtml(html);

    var newLine = '\n';

    html = html.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '');
    html = html.replace(/<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gi, '');

    html = html.replace(/<(\/p|\/div|\/li|\/h\\d|br)\s*\/?>/ig, newLine);
    // remove all tags
    html = html.replace(/<[A-Za-z\!\/][^<>]*>/ig, '');
    // remove duplicated white space
    html = html.replace(/\&nbsp;/g, ' ');
    html = html.trim().replace(/ +/g, ' ');
    // remove duplicated end lines
    html = html.replace(/ \n/g, newLine);
    html = html.replace(/\n /g, newLine);
    html = html.replace(/\n+/g, newLine);

    return html;
};
