'use strict';

var url = require('url'),
    cheerio = require('cheerio');

/**
 * A helper function that sanitizes an article content page based on some website-specific configurations
 *
 * @param  {CheerioObject} $content cheerio object (jquery like object)
 * @return {String}          Sanitized html
 */
module.exports = function ($content, articleLink) {
    var $selector, html = '';


    // remove unwanted tags
    $selector = $content.find('script').remove();
    $selector = $content.find('iframe').remove();
    $selector = $content.find('object').remove();
    $selector = $content.find('embed').remove();
    $selector = $content.find('marquee').remove();

    function buildResourceUrl(link) {
        if (!/^http/.test(link)) {
            link = url.resolve(articleLink, link);
        }
        return link;
    }

    // convert relative links to absolute link
    $content.find('img[src]').each(function() {
        cheerio(this).attr('src', buildResourceUrl(cheerio(this).attr('src')));
    });

    $content.find('link[href]').each(function() {
        cheerio(this).attr('href', buildResourceUrl(cheerio(this).attr('href')));
    });

    // remove style attributes
    $content.find('[style]').removeAttr('style');
    $content.find('font').removeAttr('size').removeAttr('face');
    $content.find('div:empty, p:empty, span:empty').remove();

    $selector = $content.find('p').filter(function () {
        return /^\s+$/.test(cheerio(this).text());
    }).remove();

    // add attribute [rf-ext-link] to external links
    $content.find('a[href]').each(function () {
        var $this = cheerio(this);
        $this.attr('rf-ext-link', $this.attr('href')).removeAttr('href');
    });

    html = $content.html();
    return html;
};
