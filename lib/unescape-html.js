'use strict';

module.exports = function (unsafe) {
    return unsafe
        .replace(/&ndash;/g, '-')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '\"')
        .replace(/&#039;/g, '\'');
};
